install(
    DIRECTORY location_service/com
    DESTINATION include/lomiri-location-service-${LOMIRI_LOCATION_SERVICE_VERSION_MAJOR}
)
