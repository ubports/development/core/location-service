# Korean translation for location-service
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the location-service package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: location-service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-21 21:51+0000\n"
"PO-Revision-Date: 2024-05-16 17:56+0000\n"
"Last-Translator: Daniel Soohan Park <soobak0313@outlook.com>\n"
"Language-Team: Korean <https://hosted.weblate.org/projects/lomiri/location-"
"service/ko/>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.6-dev\n"
"X-Launchpad-Export-Date: 2016-09-29 06:44+0000\n"

#: src/location_service/com/lomiri/location/service/trust_store_permission_manager.cpp:96
msgid "wants to access your current location."
msgstr "현재 위치에 접근하려고 합니다."

#~ msgid "An unconfined application wants to access your current location."
#~ msgstr "확인할 수 없는 프로그램이 사용자의 현재 위치에 접근하려 합니다."

#, boost-format
#~ msgid "%1% wants to access your current location."
#~ msgstr "%1%이(가) 사용자의 현재 위치에 접근하려 합니다."
